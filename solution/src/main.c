
#include "include/util.h"
#include "include/image_rotation.h"
#include <stdlib.h>
#include <string.h>

static const char *const STATUSES[] = {
        [READ_OK] = "READ_OK",
        [READ_INVALID_SIGNATURE]="READ_INVALID_SIGNATURE",
        [READ_INVALID_BITS]="READ_INVALID_BITS",
        [READ_INVALID_HEADER]="READ_INVALID_HEADER",
        [READ_FAILED_HEADER]="READ_FAILED_HEADER",
        [IMAGE_NULL]="IMAGE_NULL",
        [READ_INVALID_PIXEL_LINE]="READ_INVALID_PIXEL_LINE",
        [FAILED_TO_SKIP_TRASH]="FAILED_TO_SKIP_TRASH"
};
static const char *const WRITE_STATUSES[] = {
        [WRITE_OK] = "WRITE_OK",
        [WRITE_ERROR]="WRITE_ERROR",
        [WRITE_FAILED_PIXELS]="WRITE_FAILED_PIXELS",
        [WRITE_FAILED_TRASH]="WRITE_FAILED_TRASH",
        [WRITE_FAILED_HEADER]="WRITE_FAILED_HEADER"
};

int main(int argc, char **argv) {
    if (argc != 4) {
        fprintf(stderr,
                " current argc-%d \n necessary format: ./image-transformer "
                "<source-image> <transformed-image> <angle>\n",
                argc);
        return 1;
    }
    uint64_t angle = (atoi(argv[3]) + 360) % 360;
    if (angle != 0 && angle % 90 != 0) {
        fprintf(stderr, "Incorrect angle-%s. Angle must be one of the following:0, 90, -90, 180, -180, 270, -270\n",
                argv[3]);
        return 1;
    }
    struct file file = new_file(argv[1], "rb");
    if (!file.valid) {
        fprintf(stderr, "\nCannot open source bmp file \"%s\"\n", argv[1]);
        fclose_file(file);
        return 1;
    }
    struct file output_file = new_file(argv[2], "wb");
    if (!output_file.valid) {
        fprintf(stderr, "\nCannot open output bmp file \"%s\"\n", argv[2]);
        fclose_file(output_file);
        fclose_file(file);
        return 1;
    }

    struct image img = {0};
    enum read_status status = read_bpm_default(file.open_file, &img);
    if (status != READ_OK) {
        fclose_file(output_file);
        fclose_file(file);
        image_destroy(&img);
        fprintf(stderr, "Program failed during reading source bpm file with status %s\n", STATUSES[status]);
        return 1;
    }
    size_t count = (4 - angle / 90) % 4;
    for (size_t i = 0; i < count; i++) {
        struct image rotated = rotate(img);
        image_destroy(&img);
        img = rotated;
    }
    enum write_status status_write = write_bpm_default(output_file.open_file,&img);
    if (status_write != WRITE_OK) {
        fprintf(stderr, "Program failed during writing in output bpm file with status %s\n", WRITE_STATUSES[status_write]);
        image_destroy(&img);
        fclose_file(output_file);
        fclose_file(file);
        return 1;
    }

    fclose_file(output_file);
    fclose_file(file);
    image_destroy(&img);

    return 0;
}
