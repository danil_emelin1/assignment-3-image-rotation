//module for working with files.
#include "include/bmp.h"
struct file {
    FILE *open_file;
    bool valid;
};

struct file new_empty_file(void) {
    return (struct file) {0};
}

struct file new_file(char *fileName, char *privileges) {

    struct file file = new_empty_file();
    file.open_file = fopen(fileName, privileges);
    if (file.open_file)file.valid = true;
    return file;
}

bool fclose_file(struct file file) {
    if (!file.valid)
        return false;
    int res = fclose(file.open_file);
    if (!res)
        return true;
    else
        return false;
}


static enum read_status read_bpm_wrapper(FILE *source_file,
                                         struct image *img,
                                         enum read_status(read_bpm)(FILE *, struct image *)) {
    return read_bpm(source_file, img);
}

static enum write_status write_bpm_wrapper(FILE *output_file,
                                         struct image *img,
                                         enum write_status(write_bpm)(FILE *, struct image *)) {
    return write_bpm(output_file, img);
}
enum write_status write_bpm_default(FILE *output_file, struct image *img) {
    return write_bpm_wrapper(output_file, img, to_bmp);
}


enum read_status read_bpm_default(FILE *source_file, struct image *img) {
    return read_bpm_wrapper(source_file, img, from_bmp);
}
