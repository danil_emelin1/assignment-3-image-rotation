#include "include/image.h"

struct image rotate(struct image  source) {
    struct image image = image_create(source.height, source.width);
    if (!image.data) return (struct image){0};
    for (uint64_t i = 0; i < source.height; i++) {
        for (uint64_t j = 0; j < source.width; j++) {
            image.data[source.height * (j + 1) - i - 1] =
                    source.data[source.width * i + j];
        }
    }
    return image;
}
