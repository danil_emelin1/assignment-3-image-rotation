#ifndef UTIL_H
#define UTIL_H
#include "bmp.h"


struct file{
    FILE *open_file;
    bool valid;
};
struct file new_empty_file(void);
struct file new_file(char *,char * );
bool fclose_file(struct file );
enum read_status read_bpm_default(FILE *,struct image *);
enum write_status write_bpm_default(FILE *, struct image *);
#endif
