#include "include/bmp.h"
#define BMP_USED_COLOR 0
#define BMP_TYPE 19778
#define BMP_PLANES 1
#define BMP_COLOR_DEPTH 24
#define BMP_RESERVED 0
#define BMP_XPELS_PER_METER 0
#define BMP_COMPRESSION 0
#define BMP_IMPORTANT 0
#define BMP_INFO 40


static struct bmp_header create_bmp_header(struct image *const image) {
    size_t width = image->width;
    size_t height = image->height;
    uint32_t image_byte_size = (sizeof(struct pixel) * width + count_padding(width)) * height;
    uint32_t bmp_header_byte_size = sizeof(struct bmp_header);
    return (struct bmp_header) {
            .bfType = BMP_TYPE,
            .bfileSize = bmp_header_byte_size + image_byte_size,
            .bOffBits = bmp_header_byte_size,
            .biSizeImage = image_byte_size,
            .bfReserved = BMP_RESERVED,
            .biCompression = BMP_COMPRESSION,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = BMP_PLANES,
            .biSize = BMP_INFO,
            .biBitCount = BMP_COLOR_DEPTH,
            .biXPelsPerMeter = BMP_XPELS_PER_METER,
            .biYPelsPerMeter = BMP_XPELS_PER_METER,
            .biClrUsed = BMP_USED_COLOR,
            .biClrImportant = BMP_IMPORTANT
    };
}
static enum read_status read_pixels(FILE *file, struct image *image) {
    size_t width = image->width;
    size_t height = image->height;
    struct pixel *pixels = image->data;
    uint8_t padding_size = count_padding(width);

    for (size_t i = 0; i < height; i++) {
        size_t pixels_in_row;
        size_t padding;

        pixels_in_row = fread(pixels + i * width, sizeof(struct pixel), width, file);
        if (pixels_in_row != width) {
            return READ_INVALID_PIXEL_LINE;
        }
        padding = fseek(file, padding_size, SEEK_CUR);
        if (padding) {
            return FAILED_TO_SKIP_TRASH;
        }
    }
    return READ_OK;
}

static enum read_status read_bmp_header(FILE *file, struct bmp_header *header) {
    if (!header || !file) {
        return READ_FAILED_HEADER;
    }
    return fread(header, sizeof(struct bmp_header), 1, file) != 1 ?
           READ_FAILED_HEADER : READ_OK;
}


enum read_status from_bmp(FILE *const file, struct image *const image) {
    struct bmp_header bmp_header = {0};

    enum read_status header_read = read_bmp_header(file, &bmp_header);
    if (header_read != READ_OK)
        return header_read;


    *image = image_create(bmp_header.biWidth, bmp_header.biHeight);
if (!image->data) return IMAGE_NULL;

    enum read_status pixels_read = read_pixels(file, image);

    if (pixels_read != READ_OK)
        image_destroy(image);

    return pixels_read;
}

static enum write_status write_header(FILE *file, struct bmp_header *header) {
    if (!file || !header)return WRITE_ERROR;
    return fwrite(header, sizeof(struct bmp_header), 1, file) != 1 ? WRITE_FAILED_HEADER : WRITE_OK;
}
static enum write_status write_pixels(FILE *file, struct image *image) {
    size_t width = image->width;
    size_t height = image->height;
    struct pixel* image_data = image->data;

    uint8_t padding_byte_size = count_padding(width);
    uint64_t padding_value = 0;

    for (size_t i = 0; i < height; i++) {
        if (fwrite(image_data + i * width, sizeof(struct pixel), width, file) != width) {
            return WRITE_FAILED_PIXELS;
        }
        if (fwrite(&padding_value, 1, padding_byte_size, file)!= padding_byte_size) {
            return WRITE_FAILED_TRASH;
        }
    }
    return WRITE_OK;
}

enum write_status to_bmp(FILE * file, struct image *image) {
    struct bmp_header file_header = create_bmp_header(image);
    return write_header(file, &file_header) == WRITE_OK ? write_pixels(file, image) : WRITE_FAILED_HEADER;
}
