#include "include/image.h"
#include <stdlib.h>

struct image image_create(const uint64_t width, const uint64_t height) {
  struct pixel *data = calloc(sizeof(struct pixel) * width * height, 1);
    if (!data) return (struct image) {0};
    return (struct image)
            {.height = height,
                    .width = width,
                    .data =data};
}

bool image_destroy(struct image *image) {
    if (image->data == NULL) {
        return false;
    }
    free(image->data);
    return true;
}
uint64_t count_padding(size_t width) {
    uint64_t padding_size = width * sizeof(struct pixel) % 4;
    return padding_size == 0 ? 0:4-padding_size;
}

